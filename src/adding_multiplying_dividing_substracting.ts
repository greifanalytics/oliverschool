import fs from "fs"
import path from "path"
import commandLineArgs from 'command-line-args'

const OUTPUT_FILE_PATH = "./output/output.csv"
const MAX_COLUMNS = 5;

const optionDefinitions = [
    { name: 'multiplication', alias: 'm', type: Number, multiple: true },
    { name: 'adding', alias: 'a', type: Number },
    { name: 'addingMultiple', alias: 'A', type: Number },
    { name: 'substracting', alias: 's', type: Number },
    { name: 'substractingMultiple', alias: 'S', type: Number },
    { name: 'addSubMultiple', alias: 'M', type: Number },
    { name: 'deviding', alias: 'd', type: Number, multiple: true },
    { name: 'numberOfEquations', alias: 'n', type: Number, defaultValue: 20, defaultOption: true },
    { name: 'NofNumbersInaddOrSub', alias: 'N', type: Number, defaultValue: 3 },
    { name: 'help', alias: 'h', type: Boolean }
]
const options = commandLineArgs(optionDefinitions) // parse arguments

// Write help
if (options.help || (
    !options.multiplication &&
    !options.deviding &&
    !options.substracting &&
    !options.adding &&
    !options.addingMultiple &&
    !options.substractingMultiple &&
    !options.addSubMultiple &&
    !options.NofNumbersInaddOrSub)) {
    console.log(
        `\n--multiplication      -m           Number\n
        --deviding             -d           Number\n
        --adding               -a           Number\n
        --addingMultiple       -A           Number\n
        --substracting         -s           Number\n
        --substractingMultiple -S           Number\n
        --addSubMultiple       -M           Number\n
        --numberOfEquations    -n           Number, Default: 20\n
        --NofNumbersInaddOrSub -N           Number, Default: 3\n
        --help                 -h           Void\n
        \nExample:\n
        -m 7 8 9 -d 7 8 9 --adding 10 -n 100`
    )
    process.exit(0)
}

function adding(numberOfEquation: number): Array<string> {
    let equations = []

    while (equations.length < numberOfEquation) {
        let firstNumber = Math.round(Math.random() * 100) % 100
        let secondNumber = Math.round(Math.random() * 100) % 100
        if (secondNumber + firstNumber <= 100) {
            equations.push({ firstNumber: firstNumber, secondNumber: secondNumber })
        }
    }

    const result = []
    for (let equation of equations) {
        result.push(`${pad(equation.firstNumber)} + ${pad(equation.secondNumber)} = ________`)
    }
    return result
}

function addingMultiple(numberOfEquation: number, maxLimitOfNumbers: number = 8): Array<string> {
    let equations = []

    while (equations.length < numberOfEquation) {
        let n = []
        let limit = 100
        for (let i = 0; i < maxLimitOfNumbers; i++) {
            limit = Math.round(Math.random() * 100) % limit
            n[i] = limit
        }

        if (n.reduce((p, c) => { return p + c }) <= 100) {
            equations.push(n)
        }
    }

    const result: string[] = []
    for (let equation of equations) {
        result.push(`${equation.map(i => pad(i)).join(" + ")} = ________`)
    }
    return result
}

function substracting(numberOfEquation: number): Array<string> {
    let equations = []

    while (equations.length < numberOfEquation) {
        let firstNumber = Math.round(Math.random() * 100) % 100
        let secondNumber = Math.round(Math.random() * 100) % 100
        if (firstNumber - secondNumber <= 100 && firstNumber - secondNumber >= 0) {
            equations.push({ firstNumber: firstNumber, secondNumber: secondNumber })
        }
    }

    const result = []
    for (let equation of equations) {
        result.push(`${pad(equation.firstNumber)} - ${pad(equation.secondNumber)} = ________`)
    }
    return result
}

function substractingMultiple(numberOfEquation: number, maxLimitOfNumbers: number = 8): Array<string> {
    let equations = []

    while (equations.length < numberOfEquation) {
        let n = []
        let limit = 100
        for (let i = 0; i < maxLimitOfNumbers; i++) {
            n[i] = Math.round(Math.random() * 100) % limit
        }

        if (n.reduce((p, c) => { return p - c }) >= 0) {
            equations.push(n)
        }
    }

    const result: string[] = []
    for (let equation of equations) {
        result.push(`${equation.map(i => pad(i)).join(" - ")} = ________`)
    }
    return result
}

function addingSubstractingMultiple(numberOfEquation: number, maxLimitOfNumbers: number = 8): Array<string> {
    let equations = []

    while (equations.length < numberOfEquation) {
        let n = []
        let limit = 100

        for (let i = 0; i < maxLimitOfNumbers; i++) {
            if (i == 0) {
                limit = (Math.round(Math.random() * 100) % 100)
            } else {
                limit = (Math.round(Math.random() * 1000) % 200) - 100
            }
            n[i] = limit
        }

        const summary = n.reduce((p, c) => {
            if (p + c > 100 || p + c < 0) {
                return Infinity
            } else return p + c
        })
        if (summary <= 100 && summary >= 0) {
            equations.push(n)
        }
    }

    const result: string[] = []
    for (let equation of equations) {
        result.push(`${equation.map(i => {
            if (i > 0) {
                return `+ ${pad(i)}`
            } else {
                return `- ${pad(Math.abs(i))}`
            }
        }).join(" ")} = ________`.replace(/^\+\s/, ""))
    }
    return result
}

function multiplication(multiplier: number, numberOfEquation: number): Array<string> {
    let equations = []

    while (equations.length < numberOfEquation) {
        const first = Math.round(Math.random() * 10) % 11
        if (numberOfEquation <= 10) {
            if (findInEq(equations, first, multiplier))
                continue
        }
        equations.push({ firstNumber: first, secondNumber: multiplier })
    }

    const result = []
    for (let equation of equations) {
        const rnd = Math.random() * 10
        if (rnd > 5) {
            result.push(`${pad(equation.firstNumber)} . ${pad(equation.secondNumber)} = ________`)
        } else {
            result.push(`${pad(equation.secondNumber)} . ${pad(equation.firstNumber)} = ________`)
        }
    }
    return result
}

function devide(multiplier: number, numberOfEquation: number): Array<string> {
    let equations = []

    while (equations.length < numberOfEquation) {
        const first = Math.round(Math.random() * 100) % 100
        if (first % multiplier == 0 && first / multiplier <= 10) {
            if (numberOfEquation <= 10) {
                if (findInEq(equations, first, multiplier))
                    continue
            }
            equations.push({ firstNumber: first, secondNumber: multiplier })
        }
    }

    const result = []
    for (let equation of equations) {
        result.push(`${pad(equation.firstNumber)} : ${pad(equation.secondNumber)} = ________`)
    }
    return result
}

function findInEq(equations: Array<Equation>, first: number, second: number) {
    return equations.find(item => { return item.firstNumber === first && item.secondNumber === second }) !== undefined
}

/**
 * Keep shuffling until no 2 equations in same column and next row is same
 * @param {Array} a 
 */
function shuffle(a: Array<string>): Array<string> {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }

    while (
        a.some((current, index) => {
            if (index + MAX_COLUMNS >= a.length)
                return false
            if (a[index] === a[index + MAX_COLUMNS]) {
                const j = Math.floor(Math.random() * (index + 1));
                [a[index], a[j]] = [a[j], a[index]];
                return true
            }
            return false
        })
    ) { }

    return a;
}

function saveFile(data: string) {
    const dirPath = path.dirname(OUTPUT_FILE_PATH)
    if (!fs.existsSync(dirPath)) {
        fs.mkdirSync(dirPath)
    }
    const file = path.parse(OUTPUT_FILE_PATH)
    fs.writeFileSync(dirPath + `/${file.name}-${Date.now()}${file.ext}`, data);
}

function generateCsvWithRows(data: Array<string>, maxRows: number): string {
    let line = []
    let lines = []
    for (let i = 0; i <= data.length; i++) {
        if (0 == (i % maxRows)) {
            lines.push(line.join(","))
            line = []
        }
        line.push(data[i])
    }
    lines.push(line.join(","))
    return lines.join("\n")
}

const pad = (num: number, size = 2) => String(num).padStart(size, ' ')

/**
 * Core
 */
let allTypes = options.multiplication ? options.multiplication.length : 0
allTypes += options.deviding ? options.deviding.length : 0
const numberOfEachEq: number = Math.round(options.numberOfEquations / allTypes)
const numberOfMultipleNumbersInAddOrSub: number = options.NofNumbersInaddOrSub

const mul: Array<number> = options.multiplication ? options.multiplication : []
const dev: Array<number> = options.deviding ? options.deviding : []
const add: number = options.adding ? options.adding : 0
const addMultiple: number = options.addingMultiple ? options.addingMultiple : 0
const sub: number = options.substracting ? options.substracting : 0
const subMultiple: number = options.substractingMultiple ? options.substractingMultiple : 0
const addSubMultiple: number = options.addSubMultiple ? options.addSubMultiple : 0


const equations: string[] =
    Array<string>().concat(...mul.map(n => multiplication(n, numberOfEachEq)))
        .concat(...dev.map(n =>
            devide(n, numberOfEachEq)))
        .concat(adding(add))
        .concat(substracting(sub))
        .concat(addingMultiple(addMultiple, numberOfMultipleNumbersInAddOrSub))
        .concat(substractingMultiple(subMultiple, numberOfMultipleNumbersInAddOrSub))
        .concat(addingSubstractingMultiple(addSubMultiple, numberOfMultipleNumbersInAddOrSub))

const shuffedEq = shuffle(equations)
const csvFileContent = generateCsvWithRows(shuffedEq, MAX_COLUMNS)
saveFile(csvFileContent)
console.log(csvFileContent)

interface Equation {
    firstNumber: number
    secondNumber: number
}