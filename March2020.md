
# Oliver's school plan for duration of COVID-19 quarrantine started 11/03/2020

## 12/03/2020

1. Mathematics - multiplication and division by 2
    1. started a lesson notes notebook ![notebook1](home/20200311/WhatsApp_Image_2020-03-11_at_21.05.07.jpeg)
    2. started a calculation practice notebook ![practice](home/20200311/IMG_20200312_085415.jpg) ![practice2](home/20200312/WhatsApp_Image_2020-03-12_at_08.58.35.jpeg)
2. Mathematics - Not swaping numbers while subtracting and dividing
3. Czech - root of the word explanation and practice ![czech](home/20200311/WhatsApp_Image_2020-03-11_at_21.05.22.jpeg)
    1. further progress - needs to learn this terminology
4. Czech - writting capital letters at the start of the sentece
5. Writting - Writting book for 1st grade letter "O" ![O](home/20200311/WhatsApp_Image_2020-03-11_at_21.04.54.jpeg)
6. Reading  frestyle greek mythology and Human body (Prvouka)

## 13/03/2020

1. Czech - dictation - written in separate notebook for excercises + correctly written - submitted to Jana ![image info](home/20200311/IMG_20200312_085415.jpg)
2. Czech - Practice of types of senteces - needs more work
3. Mathematics - Test division and lmultiplication by 2 - submitted to Jana
4. Mathematics - Multiplication by 3 - notes mainly and some calculations - needs more work
5. reading - Fi and Flo[^2] pages 8 and 9
6. Painting a spring flower - chose daffodil

## 14/03/2020 - 15/03/2020 - weekend at father's

1. Mathematics - 1x A4 pages of addition and subtraction to 100 ![practice](home/20200315/WhatsApp_Image_2020-03-15_at_22.04.02.jpeg)

## 16/03/2020

1. Czech - dictation from orange book Diktáty[^3] page 7 excercise 3
2. Mathematics - Checking the weekend addition and subtraction to a 100
3. Czech - root of words practice in identifying
4. Reading - Fi and Flo[^2] pages 10-11
5. "Matematika a Pozornost..."[^4]  page 2+3
6. Cvičení z pravopisu[^1] transcribe page 9 - types of sentences into a notes book for notes ![](home/20200317/WhatsApp_Image_2020-03-17_at_07.09.05.jpeg) ![Czech](home/20200317/WhatsApp_Image_2020-03-17_at_07.09.15.jpeg)
7. Mathematics - refresh multiplication by 3 - repeat the wheel out loud from Oliver's notebook.

## 17/03/2020

1. Czech -  dictation from  Diktáty[^3] page 7 excercise 1 ![dictation](home/20200317/WhatsApp_Image_2020-03-17_at_07.08.31.jpeg)
2. Mathematics - continuing addition and subtraction to 100 with multiplication by 2 and 3 excercises ![here](home/mathematicsExcercises20200317.pdf) ![result](home/20200317/WhatsApp_Image_2020-03-17_at_12.08.21.jpeg)
3. Czech - ![kratke a dlouhe samohlasky 01](school/20200317/krátké%20a%20dlouhé%20samohlásky%201.jpg) 
4. Mathematics - [pracovni list](school/20200317/prac.%20list%20matika1.docx)
5. call with Jana Jandova 
6. Reading - Fi and Flo[^2] pages 11-12
7. Czech -  Cvičení z pravopisu[^1] page 9 excercise 2 ![Czech](home/20200317/WhatsApp_Image_2020-03-17_at_17.09.39.jpeg)
8. Reading - čítanka[^5] - strana 3 ![Reading](home/20200317/WhatsApp_Image_2020-03-17_at_17.09.14.jpeg)
9. differentiation ![differentiation](home/20200318/WhatsApp_Image_2020-03-18_at_19.01.45.jpeg)


## 18/03/2020

1. Czech -  dictation from  Diktáty[^3] page 7 excercise 2 ![Czech](home/20200318/WhatsApp_Image_2020-03-18_at_19.01.45.jpeg)
2. Mathematics - multiplication by 4 - notes and calculations ![Mathematics](home/20200318/WhatsApp_Image_2020-03-18_at_19.03.28.jpeg)
3. Mathematics - [pracovni list matematika 2](school/20200317/prac.%20list%20matika2.docx) ![Mathematics](home/20200318/WhatsApp_Image_2020-03-18_at_19.06.13.jpeg)
4. Czech - ![kratke a dlouhe samohlasky 02](./school/20200317/krátké%20a%20dlouhé%20samohlásky%202.jpg) ![Czech](home/20200318/WhatsApp_Image_2020-03-18_at_19.06.21.jpeg) 
5. Czech - ![abeceda](./school/20200317/abeceda.png) ![Czech](home/20200318/WhatsApp_Image_2020-03-18_at_11.45.10.jpeg)
6. Reading - Fi and Flo[^2] pages 13-14
7. Skype session with Jana 1200


## 19/03/2020

1. Czech - dictation  Diktáty[^3] page 8 excercise 5 ![Czech](home/20200319/WhatsApp_Image_2020-03-19_at_12.16.22.jpeg)
2. CZech - Cvičení z pravopisu[^1] page 11 excercise 6 
3. Mathematics - [pracovni list matematika 3](school/20200317/prac.%20list%20matika3.docx)![Mathematics](home/20200319/WhatsApp_Image_2020-03-19_at_12.18.02.jpeg)
4. Skype session
5. ![Mathematics](home/20200319/WhatsApp_Image_2020-03-19_at_12.17.54.jpeg)

## 20/03/2020

1. Writing- training r, ř., Z,z from Písanka[^6]  - pages 13,14,21,22,23 ![13](home/20200320/WhatsApp_Image_2020-03-20_at_13.01.20.jpeg) ![14](home/20200320/WhatsApp_Image_2020-03-20_at_13.01.26.jpeg) ![22-23](home/20200320/WhatsApp_Image_2020-03-20_at_13.01.38.jpeg)
2. Mathematics - [pracovni list matematika 3](school/20200317/prac.%20list%20matika4.docx) ![mat](home/20200320/WhatsApp_Image_2020-03-20_at_13.01.12.jpeg)
3. Skype Session - v/f ![skype](home/20200320/WhatsApp%20Image%202020-03-20%20at%2009.01.04%20(1).jpeg)



## 23/03/2020

1. Czech - dictation  Diktáty[^3] page 11 excercise 1 - Capital letters ![dictation](home/20200323/WhatsApp_Image_2020-03-23_at_18.10.18.jpeg)
2. Mathematics - repetition multiplication and division by 3 and 4 - excercise ![math](home/20200323/WhatsApp_Image_2020-03-23_at_22.37.26.jpeg) ![math2](home/20200323/WhatsApp_Image_2020-03-23_at_22.37.37.jpeg)
3. Učitelka - lesson on CT 2 with small excercise 9:35 -10:30
4. Czech - Cvičení z pravopisu[^1] page 46 excercise 22 ![czech](home/20200323/WhatsApp_Image_2020-03-23_at_18.10.04.jpeg) ![czech2](home/20200323/WhatsApp_Image_2020-03-23_at_18.10.18.jpeg)
5. Mathematics - multiplication and division by 5 - notes ![multiplication5](home/20200323/WhatsApp_Image_2020-03-23_at_22.55.15.jpeg)
6. Reading - Fi and Flo[^2] pages 19-20


## 24/03/2020

1. Czech - Pracovní sešit [^6] page 40 and 43 ![page 43](home/20200324/IMG_20200324_142613.jpg) ![page 40](home/20200324/IMG_20200324_142630.jpg)
2. Mathematics - Matematika v Malíčku [^7] page 14 ex. 1 and 2, page 22 ex. 10, 11, 12 ![page22](home/20200324/IMG_20200324_144950.jpg) ![page2](home/20200324/IMG_20200324_155652.jpg)
3. skype session with Jana 13-14
4. Reading - Fi a Flo[^2] pages 21-22
5. Mathematics - repetition multiplication and division by 3 and 4 - [excercise](home/20200324/output-1584999664107.csv) ![pocitani 24/03/2020](school/20200324/pocitani20200324_1.png)
6. Czech - [Jana's excercises](school/20200323/cestina_na_tyden.docx) ![práce 24/03/2020](school/20200324/cestina_na_tyden20200324_1.png) ![práce 24/03/2020](school/20200324/cestina_na_tyden20200324_2.png) ![práce 24/03/2020](school/20200324/cestina_na_tyden20200324_3.png)

## 25/03/2020

1. Mathematics - timed test multiplication and division by 3 (20 equations unique) ![test result](home/20200325/IMG_20200325_202026.jpg)
2. Mathematics - Matematika v Malíčku [^7] page 14 ex. 3 and 4 ![test result](home/20200325/IMG_20200325_202049.jpg)
3. Czech - [Jana's excercises](school/20200323/cestina_na_tyden.docx) page 4 and 5 ![práce 25/03/2020](school/20200325/cestina_na_tyden20200325_1.png) ![práce 25/03/2020](school/20200325/cestina_na_tyden20200325_2.png) 
4. Skype with Jana 1300-1400
5. Reading - Fi a Flo[^2] pages 22-23
6. Czech - Dictation Pracovní sešit [^6] page 52 ex 1 ![dictation](home/20200325/IMG_20200325_202227.jpg)
7. Writing - Písanka 2. díl[^9] - page 23  ![T](home/20200325/IMG_20200325_202113.jpg)


## 26/03/2020

1. Czech - Dictation Diktáty[^3] page 52 excercise 3 - d and t at the end of words  ![dictate](home/20200326/IMG_20200326_095447.jpg) ![dictate](home/20200326/IMG_20200326_095454.jpg) ![dictate](home/20200326/IMG_20200326_095457.jpg) ![dictate](home/20200326/IMG_20200326_095507.jpg)
2. Mathematics - Matematika v Malíčku[^7] page 35 ex. 7 ![35/7](home/20200326/IMG_20200326_164821.jpg)
3. Mathematics - Matematika v Malíčku[^7] page 38 ex. 1 ![maths](home/20200326/IMG_20200326_164821.jpg)
4. Mathematics - [multiplication and division by 4](home/20200326/output-1585165870830.csv) ![maths](home/20200326/IMG_20200326_163513.jpg)
5. Skype with Jana 1300-1400
6. Reading - Fi a Flo[^2] pages 24-25

## 27/03/2020

1. Czech - Dictation Diktáty[^3] page 57 excercise 1 - ch and h at the end of words - needs to be verbal ![completed](home/20200327/completedWork_5.png) 
2. Czech - weekend homework pages 10-12 [here](school/20200327/CzWorkbook_Optimized.pdf) ![completed](home/20200327/completedWork_2.png) ![completed](home/20200327/completedWork_3.png)
3. Mathematics - column addition  Matematika v Malíčku[^7] page 45 ex. 4 ![completed](home/20200327/completedWork_7.png)
4. Mathematics - column addition  Matematika v Malíčku[^7] page 61 ex. 7-8 ![completed](home/20200327/completedWork_8.png)
5. Mathematics - multiplication and division by 5 [here](home/20200327/output-1585256742746.csv) ![completed](home/20200327/ completedWorkMaths_1 .png)
6. Skype with Jana 1300-1400 ![completed](home/20200327/completedWork_6.png)
7. Painting - spring
8. Reading - Fi a Flo[^2] pages 24-27

## 30/03/2020

1. Mathematics - timed test multiplication and division by 4 ![completed](home/20200330/completedExcercises20200330_1.png)
2. Czech - Pracovní sešit [^6] page 58 in the middle - i/í y-ý ![completed](home/20200330/completedExcercises20200330_3.png)
3. Czech - Dictation Diktáty[^3] page 60 excercise 1 f or v at the end of words ![completed](home/20200330/completedExcercises20200330_5.png) 
4. Maths - Revision - multiplication and vision by 5 ![completed](home/20200330/completedExcercises20200330_2.png)
5. Mathematics - Multiplication by 1
6. Mathematics - Matematika v Malíčku[^7] page 38 ex. 2 ![completed](home/20200330/completedExcercises20200330_4.png)
7. PED English with Alun ![completed](home/20200330/completedExcercises20200330_6.png) ![completed](home/20200330/completedExcercises20200330_7.png) 
8. Reading - Fi a Flo[^2] pages 28-29
9. Going outside for excercise


## [MORE...April](April2020.md)





## Details of [books].

[^1]: Cvičení z pravopisu pro malé školáky pro 2. - 5. ročník
[^2]: Fi a Flo - Vědecký i fantastický příběh o skloňování podstatných jmen
[^3]: Diktáty a pravopisná cvičení pro 2. ročník ZŠ
[^4]: Matematika a Pozornost SPU
[^5]: Dyslektická čítanka pro 2-3 ročník
[^6]: Písanka 3. díl pro 1. ročník základní školy
[^7]: Matematika v Malíčku pro 2. třídu
[^8]: Pracovní sešit pro nápravu vývojových poruch učení v českém jazyce I
[^9]: Písanka 2. díl pro 1. ročník základní školy
[^10]: Matematika pro 2. ro4n9k zakaldn9 3koly 1. díl



[books]: https://bitbucket.org/greifanalytics/oliverschool/src/master/books.md