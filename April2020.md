# Oliver's school plan for duration of COVID-19 quarrantine started continuing from [March 2020](March2020.md)

## 01/04/2020

1. Mathematics - timed test multiplication and division by 5 ![completed](home/20200401/completedWork20200401_1.png) 
2. Czech - Pracovní sešit [^6] page 58 in the bottom - select acented word wehre appropriate ![completed](home/20200401/completedWork20200401_2.png) 
3. Czech - Dictation - Cvičení[^1] page 50 excersise 28 ![completed](home/20200401/completedWork20200401_3.png) ![completed](home/20200401/completedWork20200401_4.png) 
3. Czech - Grammar u/ú/ů- Cvičení[^1] page 15 excercise 2 ![completed](home/20200401/completedWork20200401_8.png) 
6. Mathematics - Matematika v Malíčku[^7] page 51 ex. 14 ![completed](home/20200401/completedWork20200401_5.png)
7. Mathematics - Matematika Fraus[^10] 45/1 ![completed](home/20200401/completedWork20200401_7.png)
8. Jana 1300-1400
9. PED english - 12-13


## 2/4/2020

1. Czech - Dictation - Cvičení[^1] page 18 excersise 5 ![completed](home/20200402/completedWork20200402_2.png)
2. Czech - Grammar ě - Cvičení[^1] page 21 excersise 2 ![completed](home/20200402/completedWork20200402_4.png)
3. Czech - Grammar y/ý/i/í - Cvičení[^1] page 43 excersise 12 ![completed](home/20200402/completedWork20200402_6.png)
4. Mathematics - Matematika v Malíčku[^7] addition page 39 ex. 4 ![completed](home/20200402/completedWork20200402_8.png)
5. Mathematics - Matematika v Malíčku[^7] subtraction page 37 ex. 10 ![completed](home/20200402/completedWork20200402_7.png)
6. Mathematics - Matematika v Malíčku[^7] multiplication and division page 58 ex. 10-11 ![completed](home/20200402/completedWork20200402_5.png)
7. Jana - 13-14 ![completed](home/20200402/completedWork20200402_3.png)
8. taking out rubish - paper

## 03/04/2020

1. Mathematics - timed test multiplication and division by 1-5 - 40 equations ![completed](home/20200403/testResult_1.png)
2. Reading - čítanka[^5] page 4/5 Reading accents ![completed](home/20200403/completedWork20200403_2.png) ![completed](home/20200403/completedWork20200403_3.png)
3. Czech - Diktation -  Cvičení[^1] page 35 excersise 3 ![completed](home/20200403/completedWork20200403_1.png)
4. Czech - excercise on hard y and soft i. https://www.pravopisne.cz/2012/01/pravidla-samohlasky-a-souhlasky/
5. Czech - y/i https://skolakov.eu/cesky-jazyk/2-trida/souhlasky/procvicujeme/diktaty1.htm
6. Art - drawing according to music using chalk - Kandinsky https://en.wikipedia.org/wiki/Wassily_Kandinsky 
7. Ucitelka TV
8. Google classroom ů/ú

## 06/04/2020

1. Mathematics - Matematika v Malíčku[^7] brackets page 52  ![completed](home/20200406/completedWork20200406_3.png)
2. Czech - notes on ú/ů  ![completed](home/20200406/completedWork20200406_1.png)
3. Czech - Dictation Diktáty[^3] page 25 excercise 1 ![completed](home/20200406/completedWork20200406_2.png)
5. Czech - Pracovní sešit[^11] page 27  
6. Jana 13-14
7. Mathematics - multiplication and division 2-5 -  ![completed](home/20200406/completedWork20200406_4.png)
8. Czech - school ntoes on czech (school/20200406/worrk20200406.pdf)
9. PED academy - 12-13 - English

## 07/04/2020


1. Czech - Dictation Diktáty[^3] page 15 excercise 2 ![completed](home/20200407/completedWork20200407_1.png)
2. Czech - notes on y/ý ![completed](home/20200407/completedWork20200407_2.png)
3. Mathematics - Matematika[^12] page 7 ![completed](home/20200407/completedWork20200407_3.png)
8. PED 1300-1440
8. Reading - Fi a Flo[^2] pages 30-34 

## 08/04/2020

1. Czech - Dictation Diktáty[^3] page 18 excercise 2 ![completed](home/20200408/completedWork20200408_2.png)
2. Mathematics - how to use compass for drawing circles - drawing of bubbles
3. Czech - school notes on i/í (school/20200406/worrk20200406.pdf) ![completed](home/20200408/completedWork20200408_1.png)
4. Mathematics - Matematika[^12] page 8 ex. 5, 6 and page 9 ![completed](home/20200408/completedWork20200408_4.png) ![completed](home/20200408/completedWork20200408_5.png)
5. Jana 13-14 ![completed](home/20200408/completedWork20200408_3.png)
6. PED Alun 78-79 pages 

## 09/04/2020

1. Czech - Dictation Diktáty[^3] page 17 excercise 1   ![completed](home/20200409/completedWork20200409_2.png)
2. Mathematics - Multiplication and division by 6 ![completed](home/20200409/completedWork20200409_3.png)
3. Mathematics - Matematika[^13] page 9 ex. 4, 6 and page 10 ex. 1, 4, 6, 8 page 12 ex. 4 ![completed](home/20200409/completedWork20200409_4.png) ![completed](home/20200409/completedWork20200409_5.png)
4. Jana 13-1400 ![completed](home/20200409/completedWork20200409_1.png)
5. Czech - school excercises ![completed](school/20200409/completedWork20200409_1.png) ![completed](school/20200409/completedWork20200409_2.png) ![completed](school/20200409/completedWork20200409_3.png) ![completed](school/20200409/completedWork20200409_4.png)

## 14/04/2020

1. Czech - Dictation Diktáty[^3] page 21 excercise 3 ![completed](home/20200414/completedWork20200414_1.png) ![completed](home/20200414/completedWork20200414_2.png)
2. Czech - [homework](school/20200412/cetinaNaTyden.pdf) ![completed](home/20200414/completedWork20200414_4.png)
3. Mathematics - multiplication and division by 6 ![completed](home/20200414/completedWork20200414_3.png)
4. Maths and Czech - Google Classroom - see there
5. Uci-telka CT 2 - Maths, Ceske baje a povesti - Lev

## 15/04/2020

1. Czech - diktat - Vtipy ![completed](home/20200415/completedWork20200415_5.png) ![completed](home/20200415/completedWork20200415_6.png)
2. Maths - multiplication and division by 6 ![completed](home/20200415/completedWork20200415_7.png)
3. Czech - [homework](school/20200412/cetinaNaTyden.pdf)
![completed](home/20200415/completedWork20200415_1.png) ![completed](home/20200415/completedWork20200415_2.png)
![completed](home/20200415/completedWork20200415_3.png)
![completed](home/20200415/completedWork20200415_4.png)

## 16/4/2020

1. Czech - Dictation Diktáty[^3] page 67 excercise 1 ![completed](home/20200416/completedWork20200416_1.png)
2. Mathematics - Multiplication and division by 7 - notes
3. Mathematics - Matematika[^13] page 10 excercise 1, 4, 5, 6, 7, 8 ![completed](home/20200416/completedWork20200416_7.png) ![completed](home/20200416/completedWork20200416_8.png)
3. CZech - Cvicení[^1] page 41 ex 5 ![completed](home/20200416/completedWork20200416_2.png)
4. Czech - Schoool workbook page 18 ![completed](home/20200416/completedWork20200416_6.png)
5. Czech - ![completed](home/20200416/completedWork20200416_3.png) ![completed](home/20200416/completedWork20200416_4.png) ![completed](home/20200416/completedWork20200416_5.png)

## 17/04/2020

1. Czech - Dictation Diktáty[^3] page 21 excercise 2 ![completed](home/20200417/completedWork20200417_1.png) ![completed](home/20200417/completedWork20200417_2.png)
2. Mathematics - multiplication and division by 8 ![completed](home/20200417/completedWork20200417_3.png)
3. Mathematics - multiplication and division by 6 and 7 ![completed](home/20200417/completedWork20200417_4.png)
4. Czech - y/i ![completed](home/20200417/completedWork20200417_5.png)

## 20/04/2020

1. Czech - Dictation Diktáty[^3] page 67 excercise 2 ![completed](home/20200420/completedWork20200420_1.png) ![completed](home/20200420/completedWork20200420_2.png) ![completed](home/20200420/completedWork20200420_3.png)  
2. Mathematics - Matematika[^13] page 14 ex. 3,4,5,6; page 15 ex. 1; page 17 ex 2 ![completed](home/20200420/completedWork20200420_4.png) ![completed](home/20200420/completedWork20200420_5.png) ![completed](home/20200420/completedWork20200420_6.png)   
3. Czech - school work ![completed](home/20200420/completedWork20200420_8.png) 
4. Czech - Jana 13-14 ![completed](home/20200420/completedWork20200420_7.png) 

## 21/04/2020

1. Czech - Dictation  Diktáty[^3] page 67 excercise 3 ![completed](home/20200421/completedWork20200421_1.png) ![completed](home/20200421/completedWork20200421_2.png)
2. Mathematics - notes on multiplication and division by 9 ![completed](home/20200421/completedWork20200421_3.png)
3. Mathematics multiplication and division by 2,3,4,6,7,8 revision - on board
4. Mathematics - 5 minute timed practice division and multiplication by 2 ![completed](home/20200421/completedWork20200421_6.png)
5. Czech - school work ![completed](home/20200421/completedWork20200421_4.png) ![completed](home/20200421/completedWork20200421_5.png)
6. Czech - Google classroom
7. Reading - Fi a Flo

## 22/04/2020

1. Czech - Dictation  Diktáty[^3] page 21 excercise 1 ![completed](home/20200422/completedWork20200422_1.png) ![completed](home/20200422/completedWork20200422_2.png)
2. Czech google classroom 
3. Czech - school work ![completed](home/20200422/completedWork20200422_3.png) ![completed](home/20200422/completedWork20200422_4.png) ![completed](home/20200422/completedWork20200422_5.png) ![completed](home/20200422/completedWork20200422_7.png) ![completed](home/20200422/completedWork20200422_8.png)
4. Mathematics - multiplication and division by 6, 7, 8, 9 ![completed](home/20200422/completedWork20200422_6.png)
5. Jana 13-14

## 23/04/2020

1. Czech - Dictation  Diktáty[^3] page 26 excercise 2 ![completed](home/20200423/completedWork20200423_1.png)
2. Mathematics - multiplication and division by 6, 7, 8, 9, 10 ![completed](home/20200423/completedWork20200423_2.png)
3. Mathematics - Matematika[^13] page 20 ex. 2,3,4,5 page 21 ex. 1, 3, 4, 6, 7, 8 ![completed](home/20200423/completedWork20200423_3.png) ![completed](home/20200423/completedWork20200423_4.png)
4. Mathematics - 5 minute timed practice/race division and multiplication by 3 ![completed](home/20200423/completedWork20200423_5.png)
5. Jana 13-14 ![completed](home/20200423/completedWork20200423_6.png) ![completed](home/20200423/completedWork20200423_7.png) ![completed](home/20200423/completedWork20200423_8.png)
6. Czech - notes on types of words  ![completed](home/20200423/completedWork20200423_9.png)

## 24/04/2020

1. Czech - Dictation  Diktáty[^3] page 52 excercise 1 ![completed](home/20200424/completedWork20200424_1.png)
2. Mathematics - multiplication and division by 6, 7, 8, 9, 10 - revision 
3. Mathematics - Matematika[^13] page 22 ex. 2, 3, 4, 5, 6, 7 page 23 ex. 1, 3, 4, 5, 6, 7 ![completed](home/20200424/completedWork20200424_3.png) ![completed](home/20200424/completedWork20200424_4.png) ![completed](home/20200424/completedWork20200424_5.png)
4. Mathematics - 5 minute timed practice/race division and multiplication by 3 column 6 ![completed](home/20200424/completedWork20200424_3.png)
5. Reading - Fi a Flo

## 27/24/2020

1. Czech - Dictation  Diktáty[^3] page 25 excercise 3
2. Mathematics - 5 minutes times practice multiplication and dicision by 4 page 3 column 17
3. Mathematics - multiplication and division by 6, 7, 8, 9, 10 - excercise
4. school work  
See [here](home/20200427/completedWork20200428.pdf)


## 28/04/202

1. Writing - p and t with a new traditional pen
2. Czech - Dictation  Diktáty[^3] page 25 excercise 2
3. Mathematics - Matematika[^13] page 15 ex. 2, 3, 5, 6 page 16
4. google classroom
See [here](home/20200428/completedWork20200428.pdf)

## 29/04/2020

1. Czech - Dictation  Diktáty[^3] page 26 excercise 3
2. Mathematics - school excersices
3. Mathematics revision of multipllication and division 6-10 from the top of the head
4. Mathematics - 5 minute timed practice/race division and multiplication by 3 column 18
5. Ucitelka - TV
6. Google classroom
See [here](home/20200429/completedWork20200429.pdf)

## 30/04/2020

1. Czech - Dictation  Diktáty[^3] page 29 excercise 3
3. Mathematics - Matematika[^13] page 25
4. Mathematics - 5 minute timed practice/race  column 45
5. Jana 13-14

## Continues: [May 2020](May2020.md)


## Details of [books].

[^1]: Cvičení z pravopisu pro malé školáky pro 2. - 5. ročník
[^2]: Fi a Flo - Vědecký i fantastický příběh o skloňování podstatných jmen
[^3]: Diktáty a pravopisná cvičení pro 2. ročník ZŠ
[^4]: Matematika a Pozornost SPU
[^5]: Dyslektická čítanka pro 2-3 ročník
[^6]: Písanka 3. díl pro 1. ročník základní školy
[^7]: Matematika v Malíčku pro 2. třídu
[^8]: Pracovní sešit pro nápravu vývojových poruch učení v českém jazyce I
[^9]: Písanka 2. díl pro 1. ročník základní školy
[^10]: Matematika 2 1. díl učebnice pro základní školy
[^11]: Pracovní sešit pro nápravu vývojových poruch učení v českém jazyce II
[^12]: Matematika 6 ščítání a odčítání s přechodem desítky do 100, násobení a dělění 2,3,4
[^13]: Matematika 7 příprava na násobení a dělení 6, 7, 8, 9, 10

[books]: https://bitbucket.org/greifanalytics/oliverschool/src/master/books.md